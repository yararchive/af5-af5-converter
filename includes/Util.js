var Util = {};

// ��� ID ������, ������ � ���.
Util.ID_cache = {};

// delay � �������������
Util.sleep = function(delay) {
  var start_time = +new Date();
  var end_time = delay + +start_time;
  var current_time = null
  do {
    current_time = +new Date();
  } while (current_time <= end_time)
};

// ����������� ������ � ������(�)
Util.wrap = function(src_str, options) {
  options = options || {};
  left = !options.left && options.left !== "" ? "'" : options.left;
  right = options.right || null;
  options.db_null = Util.isDbNull(src_str) && options.db_null;
  return (options.db_null ? "NULL" : left + src_str + (right || left));
};

// ����������� ��������� ������� � �������
Util.safe = function(src_str) {
  src_str = String(src_str);
  return src_str.replace(/\'/gi, "\"");
};

// ����������� ������ �������� � ���������
Util.not_null = function(src_str, options) {
  options = options || {};
  var empty_src_str = Util.isDbNull(src_str);
  src_str = String(src_str);
  var replace_str = options.replace_str || "";
  return empty_src_str ? replace_str : src_str;
};

Util.isDbNull = function(obj) {
  obj = String(obj);
  return obj == "" || obj == null || obj == "null" || typeof obj == "undefined";
};

Util.set_fund_cache = function(options) {
  options = options || {};
  if (options.Number && options.id) {
    this.ID_cache.f[options.id]
  } else {
    return false;
  }
};

// ������� �������� ����� ���������� � ��������� ������
Util.transform_note = function(src_note, for_delete, for_add) {
  if (Util.isDbNull(src_note)) src_note = "";
  src_note = String(src_note);
  var delete_regex = new RegExp(for_delete.join("|"), "gi");
  var space_regex = new RegExp("^(\\s)+", "gi");
  src_note = src_note.replace(delete_regex, "");
  src_note = src_note.replace(space_regex, "");
  dst_note = for_add.join("\n") + "\n" + src_note;
  return dst_note;
};

// ������ �������� �������� �����
Util.prepare_inventory_structure = function(options) {
  options = options || {};
  var recipient_inventory_cls = options.isn_tree_elem;
  if (!Util.isDbNull(options.donor_isn_inventory_cls)) {
    var afis_donor = options.donor_db.query("\
      SELECT TOP 1\
        *\
      FROM\
        [" + donor_db.name + "].[dbo].[tblINVENTORY_STRUCTURE] afis\
      WHERE\
        afis.ISN_INVENTORY_CLS = " + options.donor_isn_inventory_cls);

    if (!afis_donor.EOF) {
      if (Util.not_null(afis_donor.Fields.Item("NAME")) != Config.db.recipient.inventory_structure_tree_elem &&
        Util.not_null(afis_donor.Fields.Item("NAME")) != Config.db.recipient.inventory_structure_forest_elem) {
        if (Util.isDbNull(afis_donor.Fields.Item("ISN_HIGH_INVENTORY_CLS"))) {
          var recipient_isn_high_inventory_cls = options.isn_tree_elem;
        } else {
          var recipient_isn_high_inventory_cls = Util.prepare_inventory_structure({
            "isn_tree_elem":           options.isn_tree_elem,
            "donor_isn_inventory_cls": parseInt(afis_donor.Fields.Item("ISN_HIGH_INVENTORY_CLS")),
            "recipient_isn_inventory": options.recipient_isn_inventory,
            "donor_db":                options.donor_db,
            "recipient_db":            options.recipient_db
          });
        }

        var afis_recipient = options.recipient_db.insertOrId({
          table: "[dbo].[tblINVENTORY_STRUCTURE]",
          condition:  //"FOREST_ELEM LIKE 'B'" +
                      //" AND PROTECTED LIKE 'N'" +
                      " NAME LIKE " + Util.wrap(Util.not_null(afis_donor.Fields.Item("NAME"))) +
                      " AND (ISN_INVENTORY = " + options.recipient_isn_inventory + " OR ISN_HIGH_INVENTORY_CLS = " + recipient_isn_high_inventory_cls + ")" +
                      " AND Deleted = 0",
          id_column: "Id",
          values: {
            ID:                     "NEWID()",
            OwnerID:                Util.wrap("12345678-9012-3456-7890-123456789012"),
            CreationDateTime:       "CURRENT_TIMESTAMP",
            DocID:                  Util.wrap("00000000-0100-0000-0000-000000000000"),
            RowID:                  "0",
            StatusID:               "(SELECT TOP 1 SavedState FROM [" + options.recipient_db.name + "].[dbo].[eqDocTypes] WHERE DocURL LIKE 'INVENTORYSTRUCTURE.ascx')",
            Deleted:                "0",
            ISN_INVENTORY_CLS:      "(SELECT MAX(ISN_INVENTORY_CLS) + 1 FROM [" + options.recipient_db.name + "].[dbo].[tblINVENTORY_STRUCTURE])",
            ISN_HIGH_INVENTORY_CLS: recipient_isn_high_inventory_cls,
            ISN_ARCHIVE:            "NULL",
            ISN_FUND:               "NULL",
            // � �������� ����� ������� ����� �� �� ������ ������ �� ����������� ������� �����.
            // ������ ����� ���������� ������� ������� ���� �� ISN_HIGH_INVENTORY_CLS
            ISN_INVENTORY:          options.recipient_isn_inventory,
            // ISN_INVENTORY:          "NULL",
            CODE:                   Util.wrap(afis_donor.Fields.Item("CODE"), { db_null: true }),
            NAME:                   Util.wrap(Util.not_null(afis_donor.Fields.Item("NAME"))),
            NOTE:                   Util.wrap(afis_donor.Fields.Item("NOTE"), { db_null: true }),
            FOREST_ELEM:            Util.wrap("B"),
            PROTECTED:              Util.wrap("N"),
            WEIGHT:                 Util.wrap(afis_donor.Fields.Item("WEIGHT"), { db_null: true })
          },
          select_if_exists: ["ISN_INVENTORY_CLS"],
          need_to_be_updated: Config.db.recipient.update_records
        });
        // options.recipient_db.query("UPDATE [" + options.recipient_db.name + "].[dbo].[tblINVENTORY_STRUCTURE] SET DocID = ID WHERE ISN_INVENTORY_CLS = " + afis_recipient["ISN_INVENTORY_CLS"]);
        recipient_inventory_cls = afis_recipient["ISN_INVENTORY_CLS"];
        afis_recipient = null;
      }
    }
  }
  return recipient_inventory_cls;
};