var Config = {
  db: {
    donor: {
      server: "SERVER_NAME",
      name: "DB_NAME",
      user: "USER_NAME",
      pass: "DB_PASS",
      delete_notes: ["��������", "���������"]
    },
    recipient: {
      server: "SERVER_NAME",
      name: "DB_NAME",
      user: "USER_NAME",
      pass: "DB_PASS",
      update_records: false,
      add_notes: ["��������2013"],
      inventory_structure_forest_elem: "��������� �����",
      inventory_structure_tree_elem: "..."
    },
    max_reconnects: 0, // ������� ��� ���������� ������� ����������� � ��. 0 - ����� ������� �� ���������.
    reconnect_interval: 3, // � ��������,
    // ������������ ���������� �������, ������������� � �� �� ���
    part_size: 600,
    funds_rubric: "�������� ������"
  },
  log: {
    level: ["debug", "info", "error", "critical"],
    file: ".\\log\\af5-to-af5.log",
    unique_name: false
  }
};